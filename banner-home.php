<?php if ( has_post_thumbnail() ) {?>
    <div id="page-sub-header" style="background-image:url('<?php the_post_thumbnail_url(); ?>');" >
        <?php the_excerpt(); ?>
        <hr class="white" >
    </div>
<?php }; ?>   