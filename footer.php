<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="social">
		<div class="container pt-3 pb-3 social">
			<a href="https://www.linkedin.com/in/nina-nisar-8739bb40/">
				<i class="fab fa-linkedin"></i>
			</a>
			<a href="https://www.facebook.com/nina.nisar">
				<i class="fab fa-facebook-square"></i>
			</a>
			<a href="https://www.instagram.com/ninanisar/">
				<i class="fab fa-instagram"></i>
			</a>
			<a href="mailto:hello@ninanisar.com">
				<i class="far fa-envelope"></i>	
			</a>
		</div>
	</footer>
	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
		<div class="container pt-3 pb-3">
            <div class="site-info">
                &copy; <?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>
               
			</div><!-- close .site-info -->
			<div class="legal">
				<a href="/privacy-policy">Legal notice</a>
			</div>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>