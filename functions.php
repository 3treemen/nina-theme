<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
    wp_enqueue_style( 'calendlycss', 'https://calendly.com/assets/external/widget.css');
    wp_enqueue_script(' calendlyjs', 'https://calendly.com/assets/external/widget.js', null, null, true);
    wp_enqueue_script( 'child' , get_stylesheet_directory_uri() .'/inc/assets/js/child.js' , array( 'jquery'));
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}



function zooprefix_enqueue_custom_scripts() { 
    wp_enqueue_style( 'Oswald' , 'https://fonts.googleapis.com/css?family=Abril+Fatface|EB+Garamond:400,600|Cabin|Oswald:300,400|Abel|Roboto+Condensed:300,400|Raleway:200,400&display=swap"');
}
add_action( 'wp_enqueue_scripts', 'zooprefix_enqueue_custom_scripts' ); 

function enable_page_excerpt() {
    add_post_type_support('page', array('excerpt'));
  }
add_action('init', 'enable_page_excerpt');

function meks_which_template_is_loaded() {
    if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );
 