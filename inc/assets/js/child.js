// When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("site-title").style.fontSize = "25px";
    document.getElementById("toggler").style.fontSize = "1rem";
    document.getElementById("logo").style.display = "none";
    document.getElementById("masthead").style.backgroundColor = "white";
    jQuery('.nav-link').addClass('black');
  } else {
    document.getElementById("site-title").style.fontSize = "40px";
    document.getElementById("toggler").style.fontSize = "1.5rem";
    document.getElementById("logo").style.display = "block";
    document.getElementById("masthead").style.backgroundColor = "rgba(0, 0, 0, 0.3)";
    jQuery('.nav-link').removeClass('black');
  }
}

function toggleGrow() {
    var element1 = jQuery('#grow');
    var element2 = jQuery('#work');
    if(element1.is(':visible'))
    {
        element1.fadeToggle(3000);
        element2.fadeToggle(3000);
    }
    else
    {
      element2.fadeToggle(3000);
      element1.fadeToggle(3000);
    }    
};

window.setInterval(function(){
  /// call your function here
  toggleGrow();
}, 2000);  // Change Interval here to test. For eg: 5000 for 5 sec


function toggleWork() {
  var element1 = jQuery('#grow');
  if(element1.text() == 'Grow') {
    console.log('element is grow');

    element1.fadeToggle(600);

  }
}

jQuery(document).ready(function(){
  jQuery(".calendly").click(function() {
      Calendly.showPopupWidget('https://calendly.com/ninanisar/45min');return false;

  });
});

