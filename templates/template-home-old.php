<?php
/**
 * Template Name: Homepage
 * The template for custom home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
		<section id="home-one" class="col-sm-12 col-lg-10 offset-lg-1 text-center">
		<!-- we move this quote with javascript above the first h1 -->
		<div id="banner-quote" class='quote'>'Some doors open only from <i class='kaushan'>inside</i>' - Hafiz</div>

		<!-- we insert #third with javascript into the header -->
			<div id="third">
 				<p>
					The relationships we are in in work, life and love are our biggest struggle and best
				 	place for our growth. They bring it all up. </p>
				<p>
					Are you ready to be transformed by what you are struggling with?</p>
					
				<br><a href="/connect" class='btn btn-default'>Let´s connect</a>
					
			</div>

 
			
	
		
			</section><!-- #primary -->
		</div><!-- row -->
	</div><!-- container -->
	
	<section id="home-one">
		<div class="container-fluid">
			<div class="row">
					<div class="col-sm-4 offset-sm-2 pt-5 pink">
					<h2 class="fs-3"><i class="kaushan">Hi,</i><br> I´m Nina</h2>
						<p>I´m a relational leadership coach, organizational culture consultant, and passionate mama and life partner.</p>
						<p>
							My work and this site is dedicated to empowering intentional individuals in the making (we all are!) to grow up and lotus through 
							the mud of our relationships in work, life and love.
						</p>
						<a href="/about" class='btn btn-default red'>Read more</a>
					</div>
					<div class="col-sm-5 pl-0">
					<img src="/wp-content/uploads/2019/07/nina_home2.jpg" alt="Hi, I am Nina" />

					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container-fluid grey">
		<div class="row">
			<div class="col-12">

				<div class="container">
					<div class="row">
						<div class="col-sm-12 ">
							<section id="home-two" class="big text-center">
								<p>
									You are intentional, self-aware and committed to your inner work. 
									You know where you want to be, yet are finding yourself limited 
									creating that with the people around you.
								</p>
								<p> 
									You need support to grow with the relationships you are in.
								</p>
								<h3 class="text-center">DEEP INNER WORK</h3>
								<p class="small">
									<b>I offer guidance, perspective, and developmental practice to individuals who want to 
									strengthen their relational intelligence, come into deeper connection with themselves 
									and others, create powerfully from there.</b> You're invited into a thought-provoking and creative
									relationship that potentiates new avenues for development in your life. our work together welcomes you
									into new ways of being for a richer, more easeful, powerful, and fulfilling you.
								</p>

							</section>
						</div>
					</div>
				</div>
	

			</div>
		</div>
	</div>
	
	
<?php
get_footer();
